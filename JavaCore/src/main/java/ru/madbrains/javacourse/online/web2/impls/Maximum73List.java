package ru.madbrains.javacourse.online.web2.impls;

import ru.madbrains.javacourse.homework.part1.AdvancedList;
import ru.madbrains.javacourse.homework.part1.AuthorHolder;
import ru.madbrains.javacourse.homework.part1.SimpleList;

import java.util.Comparator;
        import java.util.Optional;
        import java.util.Random;

public class Maximum73List<T> implements AuthorHolder, AdvancedList<T>, SimpleList<T> {
    private T[] list;
    private int count = 0;

    public Maximum73List() {
        list = (T[]) new Object[10];
    }

    @Override
    public void add(T item) {
        T[] newList;
        if (count == list.length) {
            newList = (T[]) new Object[list.length*2];
            System.arraycopy(list,0, newList,0,list.length);
            list = newList;
        }
        list[count] = item;
        count++;
    }

    @Override
    public void insert(int index, T item) {
        try {
            if (index > size() - 1 || index < 0) throw new Exception();
            list[index] = item;
        } catch (Exception e) {
            System.out.println("Index [" + index + "] is outside the array.");
        }
    }

    @Override
    public void remove(int index) {
        T[] tmp = (T[]) new Object[size() - 1];
        int j = 0;

        try {
            if (index < 0 || index > size() - 1) throw new Exception();
            list[index] = null;
            for (T t : list) {
                if (t != null) {
                    tmp[j] = t;
                    j++;
                }
            }
            list = tmp;
            count--;
        } catch (Exception e) {
            System.out.println("Index [" + index + "] is outside the array.");
        }
    }

    @Override
    public Optional<T> get(int index) {
        if (index < count) return Optional.ofNullable(list[index]);
        else {
            return Optional.empty();
        }
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void addAll(SimpleList<T> simpleList) {
        Optional<T> t;
        int sizeList = size();
        int sizeSimpleList = simpleList.size();
        int newSize = sizeList + sizeSimpleList;
        T[] tmp = (T[]) new Object[newSize];
        System.arraycopy(list,0,tmp,0,size());
        int j = sizeList;
        for (int i = 0; i < sizeSimpleList; i++) {
            t = simpleList.get(i);
            if (t.isPresent()) {
                tmp[j] = t.get();
                j++;
            } else {
                newSize--;
            }
        }
        list = tmp;
        count = newSize;
    }

    @Override
    public int first(T item) {
        if (item != null) {
            for (int i = 0; i < list.length; i++) {
                if (item.equals(list[i])) return i;
            }
        }
        return -1;
    }

    @Override
    public int last(T item) {
        if (item != null) {
            for (int i = list.length - 1; i >= 0; i--) {
                if (item.equals(list[i])) return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(T item) {
        for (T t : list) {
            if (item.equals(t)) return true;
        }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public AdvancedList<T> shuffle() {
        Maximum73List<T> shuffleList = new Maximum73List<>();
        shuffleList.addAll(this);
        Random r = new Random();
        Optional<T> t;
        Optional<T> x;
        int index;
        for (int i = 0; i<shuffleList.size(); i++) {
            index = r.nextInt(i+1);
            t = shuffleList.get(index);
            x = shuffleList.get(i);
            if (x.isPresent()) shuffleList.insert(index, x.get());
            if (t.isPresent()) shuffleList.insert(i, t.get());
        }
        return shuffleList;
    }

    public void showAllElementsOfList() {
        for (int i = 0; i<size(); i++) {
            System.out.println(list[i]);
        }
    }

    private T getT(int index) {
        Optional<T> t = this.get(index);
        return t.orElse(null);
    }

    @Override
    public AdvancedList<T> sort(Comparator<T> comparator) {
        Maximum73List<T> sortList = new Maximum73List<>();
        sortList.addAll(this);
        sortList.quickSort(0,sortList.size() - 1, comparator);
        return sortList;
    }

    private void quickSort(int leftBorder, int rightBorder, Comparator<T> c) {
        int leftMarker = leftBorder;
        int rightMarker = rightBorder;
        T pivot = this.getT((leftMarker + rightMarker) / 2);
        do {
            while (c.compare(this.getT(leftMarker), pivot) < 0) {
                leftMarker++;
            }
            while (c.compare(this.getT(rightMarker), pivot) > 0){
                rightMarker--;
            }
            if (leftMarker <= rightMarker) {
                if (leftMarker < rightMarker) {
                    T tmp = this.getT(leftMarker);
                    this.insert(leftMarker, this.getT(rightMarker));
                    this.insert(rightMarker, tmp);
                }
                leftMarker++;
                rightMarker--;
            }
        } while (leftMarker <= rightMarker);

        if (leftMarker < rightBorder) quickSort(leftMarker, rightBorder, c);
        if (leftBorder < rightMarker) quickSort(leftBorder, rightMarker, c);
    }

    @Override
    public String author() {
        return "Max Sunoplya.";
    }
}
