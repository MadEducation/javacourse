package ru.madbrains.javacourse.online.web2;

import ru.madbrains.javacourse.homework.part1.AdvancedList;
import ru.madbrains.javacourse.homework.part1.AuthorHolder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Test {
    private static List<String> classList = List.of(
            "ru.madbrains.javacourse.online.web2.impls.Mechtatell.MechtatellList",
            "ru.madbrains.javacourse.online.web2.impls.mozg1984.MyList",
            "ru.madbrains.javacourse.online.web2.impls.ElizabethList",
            "ru.madbrains.javacourse.online.web2.impls.GURList",
            "ru.madbrains.javacourse.online.web2.impls.IlyaList",
            "ru.madbrains.javacourse.online.web2.impls.Lm201111List",
            "ru.madbrains.javacourse.online.web2.impls.Maximum73List",
            "ru.madbrains.javacourse.online.web2.impls.PrideList",
            "ru.madbrains.javacourse.online.web2.impls.Serd1986List",
            "ru.madbrains.javacourse.online.web2.impls.Tdsk213List",
            "ru.madbrains.javacourse.online.web2.impls.VibrissList"
//            "ru.madbrains.javacourse.online.web2.impls.WithLove1337List"
    );

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        List<?> collect = classList.stream()
                .map(s -> {
                    try {
                        return Class.forName(s).getConstructor();
                    } catch (NoSuchMethodException | ClassNotFoundException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .map(constructor -> {
                    try {
                        return constructor.newInstance();
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .filter(o -> o instanceof AuthorHolder)
                .filter(o -> o instanceof AdvancedList)
                .collect(Collectors.toList());

        List<AuthorHolder> lists = (List<AuthorHolder>) collect;
        System.out.println(lists);

        Map<String, AdvancedList<String>> advancedListMap = lists.stream()
                .collect(Collectors.toMap(AuthorHolder::author, Function.identity()))
                .entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> (AdvancedList<String>) entry.getValue()));

        System.out.println(advancedListMap);


        advancedListMap.forEach((key, value) -> {
            value.add("123");
            value.add("123");
            value.add("123");
            value.add("123");
            value.add("123");

            System.out.println(key + ": " + value.sort(String::compareTo));
        });

    }
}
