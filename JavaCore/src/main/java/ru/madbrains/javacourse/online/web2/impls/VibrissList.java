package ru.madbrains.javacourse.online.web2.impls;

import ru.madbrains.javacourse.homework.part1.AdvancedList;
import ru.madbrains.javacourse.homework.part1.AuthorHolder;
import ru.madbrains.javacourse.homework.part1.SimpleList;

import java.util.Comparator;
import java.util.Optional;
import java.util.Random;


public class VibrissList<T> implements AdvancedList<T>, AuthorHolder {

    private static final String AUTHOR = "Maxim O.";

    private Object[] array;
    private int initSize;
    private int resizeMultiplier;
    private int counter;

    public VibrissList() {
        this.initSize = 2;
        this.resizeMultiplier = 2;
        this.counter = 0;
        this.array = new Object[this.initSize];
    }

    @Override
    public String author() {
        return AUTHOR;
    }

    @Override
    public void add(T item) {
        if (this.counter == this.array.length) {
            enlargeArray();
        }

        this.array[this.counter++] = item;
    }

    @Override
    public void insert(int index, T item) throws Exception {
        if (index > this.counter || index < 0) {
            throw new Exception("index is out of bounds");
        }

        add(null);

        for (int i = this.counter - 1; i >= index + 1; i--) {
            this.array[i] = this.array[i - 1];
        }

        this.array[index] = item;
    }

    @Override
    public void remove(int index) throws Exception {
        if (index >= this.counter || index < 0) {
            throw new Exception("index is out of bounds");
        }

        for (int i = index; i < this.counter - 1; i++) {
            this.array[i] = this.array[i + 1];
        }

        this.array[--this.counter] = null;
    }

    @Override
    public Optional<T> get(int index) {
        if (index >= this.counter || index < 0) {
            return Optional.empty();
        }
        return Optional.ofNullable((T) array[index]);
    }

    @Override
    public int size() {
        return this.counter;
    }

    @Override
    public void addAll(SimpleList<T> list) {
        int iterations = list.size();
        for (int i = 0; i < iterations; i++) {
            add(list.get(i).get());
        }
    }

    @Override
    public int first(T item) {
        for (int i = 0; i < this.counter; i++) {
            if (get(i).equals(Optional.ofNullable(item))) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public int last(T item) {
        for (int i = this.counter -1; i >= 0; i--) {
            if (get(i).equals(Optional.ofNullable(item))) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public boolean contains(T item) {
        for (int i = 0; i < this.counter - 1; i++) {
            if (get(i).equals(Optional.ofNullable(item))) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isEmpty() {
        return this.counter == 0;
    }

    @Override
    public AdvancedList<T> shuffle() {
        int [] shuffledIndexes = getShuffledIndexes(this.counter);

        AdvancedList<T> shuffledList = new VibrissList<>();
        for (int i = 0; i < shuffledIndexes.length; i++) {
            shuffledList.add(get(shuffledIndexes[i]).orElse(null));
        }

        return shuffledList;
    }

//    Изначальный вариант сортировки пузырьком:
//    @Override
//    public AdvancedList<T> sort(Comparator<T> comparator){
//        Object[] sorted = array;
//        for (int i = 0; i < this.index; i++) {
//            for (int j = i; j < this.index; j++) {
//                if (comparator.compare(get(i).get(), get(j).get()) == 1) {
//                    Object temp = sorted[i];
//                    sorted[i] = sorted[j];
//                    sorted[j] = temp;
//                }
//            }
//        }
//
//        AdvancedList<T> sortedList = new MyCollection<>();
//        for (int i = 0; i < index; i++) {
//            sortedList.add((T) sorted[i]);
//        }
//        return sortedList;
//    }

    @Override
    public AdvancedList<T> sort(Comparator<T> comparator){
        Object[] arrayToSort = array.clone();
        quickSortArray((T[]) arrayToSort, 0, counter -1, comparator);

        AdvancedList<T> sortedList = new VibrissList<>();
        for (int i = 0; i < counter; i++) {
            sortedList.add((T) arrayToSort[i]);
        }
        return sortedList;
    }

    @Override
    public String toString() {
        String output = "";
        for (int i = 0; i < counter; i++) {
            output += ("[" + i + "]: " + get(i).orElse(null) + "\n");
        }
        return output;
    }

    private void enlargeArray() {
        Object[] enlargedArray = new Object[this.array.length * resizeMultiplier];
        for (int i = 0; i < this.array.length; i++) {
            enlargedArray[i] = this.array[i];
        }
        this.array = enlargedArray;
    }

//    первоначальный вариант генерации последовательности случайных неповторяющихся чисел
//    private int[] getShuffledIndexes(int length) {
//        Random random = new Random();
//
//        int[] shuffledIndexes = new int[length];
//        shuffledIndexes[0] = random.nextInt(length);
//
//        for (int i = 1; i < length; i++) {
//            int randomNumber;
//            while (true) {
//                int count = 0;
//                randomNumber = random.nextInt(length);
//                for (int j = 0; j < i; j++) {
//                    if (randomNumber == shuffledIndexes[j]) {
//                        count++;
//                    }
//                }
//                if (count == 0) {
//                    break;
//                }
//            }
//            shuffledIndexes[i] = randomNumber;
//        }
//        return shuffledIndexes;
//    }

    private int[] getShuffledIndexes(int length) {
        //"подопытный массив", из которого будут случайным образом извлекаться значения
        //размер увеличен, чтобы не выходить из диапазона при перезаписи элементов
        int[] tempArray = new int[length + 1];
        //итоговый массив
        int[] shuffledIndexes = new int[length];

        for (int i = 0; i < tempArray.length; i++) {
            tempArray[i] = i;
        }

        Random random = new Random();

        //заполнение итогового массива
        for (int i = 0; i < length; i++) {
            //уменьшающийся диапазон выбора случайного числа
            int bound = length - i;
            //случайное число из диапазона
            int randomInt = random.nextInt(bound);
            //заполнение итогового массива значением из "подопытного массива" с
            //индексом равным сгенерированному случайному числу
            shuffledIndexes[i] = tempArray[randomInt];
            //сдвиг последующих элементов "подопытного массива" на место выбранного
            //значения со случайным индексом
            for (int j = randomInt; j < bound; j++) {
                tempArray[j]  = tempArray[j + 1]; //
            }
        }

        return shuffledIndexes;
    }

    private void quickSortArray(T[] arrayToSort, int start, int end, Comparator<T> comparator) {
        if (start < end) {
            T pivot = arrayToSort[end];
            int i = (start - 1);

            for (int j = start; j < end; j++) {
                if (comparator.compare(arrayToSort[j], pivot) < 1) {
                    i++;

                    T temp = arrayToSort[i];
                    arrayToSort[i] = arrayToSort[j];
                    arrayToSort[j] = temp;
                }
            }

            T temp = arrayToSort[i + 1];
            arrayToSort[i + 1] = arrayToSort[end];
            arrayToSort[end] = temp;

            int partitionIndex = i + 1;

            quickSortArray(arrayToSort, start, partitionIndex - 1, comparator);
            quickSortArray(arrayToSort, partitionIndex + 1, end, comparator);
        }
    }
}
