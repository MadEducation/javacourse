package ru.madbrains.javacourse.online.web1;

import ru.madbrains.javacourse.lesson3.employer.Developer;
import ru.madbrains.javacourse.lesson3.employer.Employer;

import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;


public class Example {
    private static List<Developer> developers = List.of(
            new Developer("Ivan Ivanov", 20, "Java"),
            new Developer("Petr Petrov", 30, "Java"),
            new Developer("Fedor Fedorov", 40, "Java"),
            new Developer("Alexander Alexandrov", 21, "C++"),
            new Developer("Dmitriy Dmitrov", 24, "C++"),
            new Developer("Alexey Alexeev", 28, "C++"),
            new Developer("Andrey Andreev", 38, "C++"),
            new Developer("Sergey Sergeev", 30, "C++"),
            new Developer("Vladimir Vladimirov", 22, "Python"),
            new Developer("Anton Antonov", 29, "Python"),
            new Developer("Vasiliy Vasiliev", 23, "Go"),
            new Developer("Artem Artemov", 27, "Java"),
            new Developer("Maxim Maximov", 25, "Java"),
            new Developer("Anatoliy Anatoliev", 45, "Python"),
            new Developer("Anna Annova", 18, "JavaScript"),
            new Developer("Pavel Pavlov", 36, "JavaScript"),
            new Developer("Egor Egorov", 32, "Ruby"),
            new Developer("Denis Denisov", 33, "Ruby")
    );

    public static void main(String[] args) {
        List<Integer> collect = developers.stream()
                .map(Employer::getAge)
                .collect(Collectors.toList());

        double aDouble = collect.stream()
                .mapToInt(Integer::intValue)
                .average().getAsDouble();
        System.out.println(aDouble);


//        Comparator<Developer> comparator2 = new Comparator<Developer>() {
//            @Override
//            public int compare(Developer o1, Developer o2) {
//                return 0;
//            }
//        };
        Comparator<Developer> comparator = (o1, o2) -> {
            if (o1.getAge() == o2.getAge()) {
                return 0;
            }
            return (o1.getAge() > o2.getAge()) ? -1 : 1;
        };

        List<Developer> collect1 = developers.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
        System.out.println(collect1);

        List<Developer> javaDevelopers = Example.developers.stream()
                .filter(x -> "Java".equals(x.getLanguage()))
//                .filter(x -> x.getLanguage().equals("Java"))
                .collect(Collectors.toList());
        System.out.println(javaDevelopers);

        Set<Developer> developerSet = developers.stream()
                .filter(x -> x.getAge() > 30)
                .collect(Collectors.toSet());
        System.out.println(developerSet);

        Set<String> stringSet = developers.stream()
                .map(Developer::getLanguage)
                .collect(Collectors.toSet());
        System.out.println(stringSet);

        developers.stream()
                .sorted(comparator)
                .map(Developer::getName)
                .limit(3)
                .collect(Collectors.joining(","));
        System.out.println(developers);
    }
}
