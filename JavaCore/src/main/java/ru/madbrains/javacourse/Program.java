package ru.madbrains.javacourse;

import ru.madbrains.javacourse.lesson2.Developer;
import ru.madbrains.javacourse.lesson2.Employer;
import ru.madbrains.javacourse.lesson2.MathUtils;

import java.util.List;

public class Program {
    public static void main(String[] args) {
        Employer employer = new Employer();
        employer.work();

        Developer developer = new Developer("Igor Petrov", 26);
        developer.setLanguage("Java");
        developer.work();
        developer.writeCode();

        System.out.println("result = " + MathUtils.factorial(5));
    }
}
