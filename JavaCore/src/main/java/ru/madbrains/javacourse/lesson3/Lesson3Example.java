package ru.madbrains.javacourse.lesson3;

import ru.madbrains.javacourse.lesson3.employer.*;

import java.util.Arrays;
import java.util.List;

public class Lesson3Example {
    public static void main(String[] args) {
        Employer pm = new PM("Sergey Kislyakov", 30);
        Employer qa = new QA("Yulia Shumillina", 18);
        Employer developer = new Developer("Igor Petrov", 26, "Java");

//        List<Worker> workers = Arrays.asList(pm, qa, developer);
//        workers.stream().forEach(w -> w.work());

        ITCompany company = new ITCompany("MadBrains", 100);
        company.addEntity(pm);
        company.addEntity(qa);
        company.addEntity(developer);
        company.startWork();

        ITCompany company2 = new ITCompany("MadBrains", 100);

        System.out.println("Company: " + company);
        System.out.println(company.equals(company2));
    }
}
