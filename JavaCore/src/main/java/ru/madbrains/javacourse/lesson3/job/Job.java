package ru.madbrains.javacourse.lesson3.job;

public interface Job {
    void run();
}
