package ru.madbrains.javacourse.lesson2;

public class MathUtils {
    public static long factorial(long n) {
        if (n < 2) {
            return 1;
        }

        return n * factorial(n-1);
        //5 * 4 * 3 * 2 * 1
    }
}
