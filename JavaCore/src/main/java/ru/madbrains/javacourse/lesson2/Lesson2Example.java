package ru.madbrains.javacourse.lesson2;

public class Lesson2Example {
    public static void main(String[] args) {
        Employer employer = new Employer();
        employer.setName("Sergey Kislyakov");
        employer.setAge(30);
        System.out.println(employer);
        employer.work();

        Employer employer2 = new Employer();
        employer2.setName("Yulia Shumillina");
        employer2.setAge(18);
        System.out.println(employer2);
        employer2.work();
    }
}
