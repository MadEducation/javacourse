package ru.madbrains.javacourse.lesson4;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class Example1 {
    private static int a = 0;
    private static Semaphore sem = new Semaphore(1);
    private static ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
//                    System.out.println(Thread.currentThread().getName());
                    if (a % 2 == 0) System.out.println(a);
                }
            }
        });


        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
//                    System.out.println(Thread.currentThread().getName());
                    a++;

                }
            }
        });

        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(15);
                } catch (InterruptedException e) {
                    System.out.println(e);
                }

                System.out.println("I am done");
            }
        });

        System.out.println(Thread.currentThread().getName());


//        thread.start();
//        thread2.start();
//
//        thread.join();
//        thread2.join();

        thread3.start();
        System.out.println("end");
    }
}
