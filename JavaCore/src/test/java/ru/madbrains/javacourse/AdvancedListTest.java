package ru.madbrains.javacourse;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import ru.madbrains.javacourse.homework.part1.AdvancedList;
import ru.madbrains.javacourse.homework.part1.AuthorHolder;
import ru.madbrains.javacourse.lesson3.ITCompany;
import ru.madbrains.javacourse.lesson3.employer.Developer;
import ru.madbrains.javacourse.online.web2.impls.*;
import ru.madbrains.javacourse.online.web2.impls.Mechtatell.MechtatellList;
import ru.madbrains.javacourse.online.web2.impls.mozg1984.MyList;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class AdvancedListTest {
    private static List<AdvancedList<Developer>> lists = List.of(
            new MechtatellList<>(), new MyList<>(), new ElizabethList<>(), new GURList<>(),
            new IlyaList<>(), /*new Lm201111List<>(),*/ new Maximum73List<>(), new PrideList<>(),
            new Serd1986List<>(), new Tdsk213List<>(), new VibrissList<>()/*, new WithLove1337List<>()*/
    );

    private static List<Developer> developers = new LinkedList<>();

    private static Map<String, AdvancedList<Developer>> listMap = new HashMap<>();

    @BeforeClass
    public static void setUp() {
        Random random = new Random();
        IntStream.range(0, 1000).forEach(i -> {
            int val = random.nextInt();
            developers.add(new Developer(String.valueOf(val), val, "Java"));
        });

        System.out.println(developers.size());

        lists.forEach(list -> {
            developers.forEach(list::add);
            if (list instanceof AuthorHolder) {
                String author = ((AuthorHolder) list).author();
                listMap.put(author, list);
            }
        });
    }

    @Test
    public void allLists_filled_with_data() {
        lists.forEach(list -> {
            System.out.println("check " + list.getClass().getName());
            Assert.assertFalse(list.isEmpty());
            Assert.assertEquals(list.size(), developers.size());
            Assert.assertTrue(isFilledWithData(list, developers));
            System.out.println("finished " + list.getClass().getName());
        });
    }

    @Test
    public void allLists_sort_correct() {
        List<AdvancedList<String>> lists = List.of(
                new MechtatellList<>(), new MyList<>(), new GURList<>(),
                new IlyaList<>(), new Maximum73List<>(),
                new Serd1986List<>(), new VibrissList<>()
        );

        List<String> strings = List.of("asd", "wef", "ergh", "dfgdfg");
        List<String> sortedStrings = strings.stream().sorted(String::compareTo).collect(Collectors.toList());
        lists.forEach(list -> strings.forEach(list::add));
        lists.forEach(list -> {
            AdvancedList<String> sortedList = list.sort(String::compareTo);
            System.out.println(list.getClass().getName());
            for (int i = 0; i < sortedList.size(); i++) {
                Assert.assertEquals(sortedStrings.get(i), sortedList.get(i).get());
            }
        });

        ITCompany itCompany = Mockito.mock(ITCompany.class);
        Mockito.when(itCompany.getName()).thenReturn("Mock Name");
        System.out.println(itCompany.getName());
    }

    private boolean isFilledWithData(AdvancedList<Developer> list, List<Developer> developers) {
        return developers.stream()
                .allMatch(list::contains);
    }

    @Test
    public void testSort() {
        Comparator<Developer> comparator = Comparator.comparing(Developer::getAge).thenComparing(Developer::getName);



        Map<String, AdvancedList<Developer>> filteredMap = listMap.entrySet().stream()
                .filter(entry -> Objects.nonNull(entry.getValue().sort(comparator)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<String, Double> collect = filteredMap.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        entry -> IntStream.range(0, 100).mapToLong(i -> sortTime(entry.getValue(), comparator)).average().getAsDouble()));
        System.out.println(collect);
        Map.Entry<String, Double> entry = collect.entrySet().stream().min(Map.Entry.comparingByValue()).get();
        System.out.println(entry);
    }

    private Long sortTime(AdvancedList<Developer> list, Comparator<Developer> comparator) {
        long start = System.currentTimeMillis();
        list.sort(comparator);
        long end = System.currentTimeMillis();
        return end - start;
    }
}
