package ru.madbrains.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.madbrains.spring.security.JwtProvider;
import ru.madbrains.spring.security.dto.AuthRequest;
import ru.madbrains.spring.security.dto.AuthResponse;
import ru.madbrains.spring.security.dto.RegistrationRequest;
import ru.madbrains.spring.entity.Account;
import ru.madbrains.spring.service.AccountService;

import javax.validation.Valid;

@RestController
public class AuthController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private JwtProvider jwtProvider;

    @PostMapping("/register")
    public String registerUser(@RequestBody @Valid RegistrationRequest registrationRequest) {
        accountService.saveUser(registrationRequest.toAccount());
        return "OK";
    }

    @PostMapping("/auth")
    public AuthResponse auth(@RequestBody AuthRequest request) {
        Account account = accountService.findByLoginAndPassword(request.getLogin(), request.getPassword());
        String token = jwtProvider.generateToken(account.getLogin());
        return new AuthResponse(token);
    }

    @GetMapping("/me")
    public ResponseEntity<Account> getUser() {
        Account account = (Account) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ResponseEntity.ok(account);
    }

}
