package ru.madbrains.spring.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.madbrains.spring.entity.employee.*;
import ru.madbrains.spring.dto.CompanyDTO;
import ru.madbrains.spring.dto.ITEmployeeDTO;
import ru.madbrains.spring.service.CompanyService;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/company")
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @PostMapping
    public Long createCompany(@RequestBody CompanyDTO companyDTO) {
        return companyService.createCompany(companyDTO.toCompany());
    }

    @GetMapping("/{id}")
    public CompanyDTO company(@PathVariable int id) {
        return CompanyDTO.from(companyService.getCompany(id));
    }

    @GetMapping("/my")
    public List<CompanyDTO> myCompanies() {
        return companyService.getMyCompanies().stream()
                .map(CompanyDTO::from)
                .collect(Collectors.toList());
    }

    @PostMapping("/{id}/employees/developers")
    public ResponseEntity addEmployee(@RequestBody Developer developer, @PathVariable(name = "id") int company_id) {
        log.info("add developer");
        companyService.addDeveloper(developer, company_id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}/employees/find")
    public ResponseEntity<List<ITEmployeeDTO>> getEmployeeByRole(
            @RequestParam(name = "role") ITRole role,
            @PathVariable(name = "id") int company_id) {
        log.info("get employee by role = " + role);

        List<ITEmployeeDTO> result = companyService.getEmployeesByRole(role, company_id)
                .stream()
                .map(ITEmployeeDTO::from)
                .collect(Collectors.toList());

        return ResponseEntity.ok(result);
    }
}
