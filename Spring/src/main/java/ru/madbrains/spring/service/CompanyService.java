package ru.madbrains.spring.service;

import ru.madbrains.spring.entity.ITCompany;
import ru.madbrains.spring.entity.employee.*;

import java.util.List;

public interface CompanyService {
    Long createCompany(ITCompany company);
    ITCompany getCompany(long id);
    List<ITCompany> getMyCompanies();
    void addDeveloper(Developer developer, long company_id);
    List<Employee> getEmployeesByRole(ITRole role, long company_id);
}
