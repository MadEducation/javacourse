package ru.madbrains.spring.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.madbrains.spring.aspect.LogExecutionTime;
import ru.madbrains.spring.dao.repository.AccountRepository;
import ru.madbrains.spring.entity.Account;
import ru.madbrains.spring.entity.ITCompany;
import ru.madbrains.spring.entity.employee.Developer;
import ru.madbrains.spring.entity.employee.Employee;
import ru.madbrains.spring.entity.employee.ITRole;
import ru.madbrains.spring.dao.CompanyDAO;

import java.util.List;

@Slf4j
@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private CompanyDAO companyDAO;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    @Transactional
    @LogExecutionTime
    public Long createCompany(ITCompany company) {
        Account account = (Account) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ITCompany itCompany = companyDAO.create(company);
        account.setEmployee(itCompany.getDirector());
        accountRepository.save(account);
        return itCompany.getId();
    }

    @Override
    @LogExecutionTime
    public ITCompany getCompany(long id) {
        return companyDAO.find(id);
    }

    @Override
    @Transactional
    public void addDeveloper(Developer developer, long company_id) {
        developer.setCompany(getCompany(company_id));
        companyDAO.addDeveloper(developer);
    }

    @Override
    public List<Employee> getEmployeesByRole(ITRole role, long company_id) {
        return companyDAO.getEmployeesByRole(role, company_id);
    }

    @Override
    public List<ITCompany> getMyCompanies() {
        Account account = (Account) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return companyDAO.findByDirector(account.getEmployee());
    }
}
