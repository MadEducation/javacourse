package ru.madbrains.spring.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import ru.madbrains.spring.entity.ITCompany;
import ru.madbrains.spring.entity.employee.Developer;
import ru.madbrains.spring.entity.employee.Employee;
import ru.madbrains.spring.entity.employee.ITRole;
import ru.madbrains.spring.dao.repository.CompanyRepository;
import ru.madbrains.spring.dao.repository.DeveloperRepository;
import ru.madbrains.spring.dao.repository.EmployeeRepository;

import java.util.List;

@Slf4j
@Primary
@Component
public class CompanyDAOImpl2 implements CompanyDAO {
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private DeveloperRepository developerRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public ITCompany create(ITCompany company) {
        ITCompany savedCompany = companyRepository.save(company);
        return savedCompany;
    }

    @Override
    public ITCompany find(long id) {
        return companyRepository.findById(id).orElse(null);
    }

    @Override
    public void addDeveloper(Developer developer) {
        developerRepository.save(developer);
    }

    @Override
    public List<Employee> getEmployeesByRole(ITRole role, long company_id) {
        log.info("find employees by role and company using Spring Repository");
        ITCompany company = find(company_id);
        return employeeRepository.findByRoleAndCompany(role, company);
//        return employeeRepository.findByRoleAndCompanyId(role, company_id);
//        return employeeRepository.findByRoleAndCompanyQ(role, company);
    }

    @Override
    public List<ITCompany> findByDirector(Employee<ITRole> director) {
        return companyRepository.findByDirector(director);
    }
}
