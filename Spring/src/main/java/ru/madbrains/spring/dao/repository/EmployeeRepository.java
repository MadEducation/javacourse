package ru.madbrains.spring.dao.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.madbrains.spring.entity.ITCompany;
import ru.madbrains.spring.entity.employee.Employee;
import ru.madbrains.spring.entity.employee.ITRole;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee<ITRole>, Long> {
    List<Employee> findByRoleAndCompany(ITRole role, ITCompany company);
    List<Employee> findByRoleAndCompanyId(ITRole role, long company_id);

    @Query(value = "select e from Employee e where e.role = :role and e.company = :company")
    List<Employee> findByRoleAndCompanyQ(ITRole role, ITCompany company);
}
