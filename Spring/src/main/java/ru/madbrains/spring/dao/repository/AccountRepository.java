package ru.madbrains.spring.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.madbrains.spring.entity.Account;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
    Account findByLogin(String login);
}
