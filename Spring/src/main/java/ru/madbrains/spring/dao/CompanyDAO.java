package ru.madbrains.spring.dao;

import ru.madbrains.spring.entity.ITCompany;
import ru.madbrains.spring.entity.employee.Developer;
import ru.madbrains.spring.entity.employee.Employee;
import ru.madbrains.spring.entity.employee.ITRole;

import java.util.List;

public interface CompanyDAO {
    ITCompany create(ITCompany company);
    ITCompany find(long id);
    List<ITCompany> findByDirector(Employee<ITRole> director);
    void addDeveloper(Developer developer);
    List<Employee> getEmployeesByRole(ITRole role, long company_id);
}
