package ru.madbrains.spring.dao;

import org.springframework.stereotype.Repository;
import ru.madbrains.spring.entity.ITCompany;
import ru.madbrains.spring.entity.employee.Developer;
import ru.madbrains.spring.entity.employee.Employee;
import ru.madbrains.spring.entity.employee.ITRole;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class CompanyDAOImpl implements CompanyDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ITCompany create(ITCompany company) {
        entityManager.persist(company);
        entityManager.flush();
        return company;
    }

    @Override
//    @Transactional(isolation = Isolation.DEFAULT)
    public ITCompany find(long id) {
        ITCompany company = entityManager.find(ITCompany.class, id);
//        entityManager.lock(company, LockModeType.PESSIMISTIC_READ);
        return company;
    }

    @Override
    public void addDeveloper(Developer developer) {
        entityManager.persist(developer);
    }

    @Override
    public List<Employee> getEmployeesByRole(ITRole role, long company_id) {
        List<Employee> employees = entityManager.createQuery(
                "select e from Employee e where e.role = :role and e.company = :company", Employee.class)
                .setParameter("role", role)
                .setParameter("company", find(company_id))
                .getResultList();
        return employees;
    }

    @Override
    public List<ITCompany> findByDirector(Employee<ITRole> director) {
        return null;
    }
}
