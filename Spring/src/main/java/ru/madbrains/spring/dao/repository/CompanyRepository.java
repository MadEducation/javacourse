package ru.madbrains.spring.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.madbrains.spring.entity.ITCompany;
import ru.madbrains.spring.entity.employee.Employee;
import ru.madbrains.spring.entity.employee.ITRole;

import java.util.List;

@Repository
public interface CompanyRepository extends CrudRepository<ITCompany, Long> {
    List<ITCompany> findByDirector(Employee<ITRole> director);
}
