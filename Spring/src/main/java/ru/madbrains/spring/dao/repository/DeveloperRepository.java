package ru.madbrains.spring.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.madbrains.spring.entity.employee.Developer;

@Repository
public interface DeveloperRepository extends CrudRepository<Developer, Long> {
}
