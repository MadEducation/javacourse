package ru.madbrains.spring.entity.employee;

public interface Worker {
    default void work() {
        throw new RuntimeException("Not implemented");
    }
}
