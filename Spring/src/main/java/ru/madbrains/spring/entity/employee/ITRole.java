package ru.madbrains.spring.entity.employee;

public enum ITRole {
    PM,
    QA,
    Developer,
    Director
}
