package ru.madbrains.spring.entity.employee;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.madbrains.spring.entity.ITCompany;

import javax.persistence.*;
@Entity
@Table(name = "employees")
@Inheritance(strategy = InheritanceType.JOINED)
public class Employee<T> implements Worker {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private int age;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private T role;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "company_id")
    private ITCompany company;

    public Employee() {
    }

    public Employee(String name, int age, T role) {
        this.name = name;
        this.age = age;
        this.role = role;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setRole(T role) {
        this.role = role;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return age;
    }

    public T getRole() {
        return role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ITCompany getCompany() {
        return company;
    }

    public void setCompany(ITCompany company) {
        this.company = company;
    }

    public static void someMethod() {
        System.out.println("method1");
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", role=" + role +
                '}';
    }
}
