package ru.madbrains.online.web5;

public interface Stringifier<T> {
    String stringify(T obj);
}
