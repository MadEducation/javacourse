package ru.madbrains.online.web5;

import groovy.lang.GroovyClassLoader;
import org.springframework.stereotype.Component;
import ru.madbrains.spring.entity.employee.Developer;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class DeveloperComponent {
    private static List<Developer> developers = List.of(
            new Developer("Ivan Ivanov", 20, "Java"),
            new Developer("Petr Petrov", 30, "Python"),
            new Developer("Fedor Fedorov", 40, "JavaScript"),
            new Developer("Alexander Alexandrov", 21, "C++"),
            new Developer("Dmitriy Dmitrov", 24, "GO"),
            new Developer("Alexey Alexeev", 28, "Ruby")
    );

    private final GroovyClassLoader loader = new GroovyClassLoader();
    private AtomicReference<Stringifier<Developer>> stringifier;
    private final Random r = new Random();

    public void load(String groovy) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Object o = loader.parseClass(groovy).getDeclaredConstructor().newInstance();
        if (o instanceof Stringifier) {
            Stringifier<Developer> stringifier = (Stringifier<Developer>) o;
            this.stringifier.set(stringifier);
        }
    }

    public String execute() {
        int index = r.nextInt(developers.size());
        Developer developer = developers.get(index);

        if (Objects.isNull(stringifier.get())) {
            return "";
        }

        return stringifier.get().stringify(developer);
    }
}
