package ru.madbrains.online.web5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.InvocationTargetException;

@Controller
@RequestMapping("/script")
public class ScriptController {
    @Autowired
    private DeveloperComponent component;

    @PostMapping("/load")
    public ResponseEntity load(@RequestBody String groovy) {
        try {
            component.load(groovy);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/execute")
    public ResponseEntity<String> execute() {
        String result = component.execute();
        return ResponseEntity.ok(result);
    }
}
